//Простой вывод данных из коллекции в консоли

const mongoClient = require("mongodb").MongoClient;
var express = require("express");
var app = express();

app.get("/", function(request, response){
    console.log("test application using node.js and MongoDB");
    
    mongoClient.connect("mongodb://localhost:27017/", function(err, client){
        
        const db = client.db("musictest");
        const collection = db.collection("bands");
    
        if(err){
            return console.log(err);
        }

        collection.find().toArray(function(err, results){

            console.log(results);
            client.close();
        })
    
        client.close();
    });

    response.send("<h2>test application using node.js and MongoDB</h2>");
});

app.listen(3000);


