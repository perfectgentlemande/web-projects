var express = require("express");
var app = express();

app.use(express.static('public'));

var path = require('path');
var public = path.join(__dirname, 'public');

app.get("/", function(request, response){
    res.sendFile(path.join(public, 'index.html'));
});

app.listen(3000);
console.log("Сервер начал прослушивание запросов на порту 3000");